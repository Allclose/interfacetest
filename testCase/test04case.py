#单接口测试用例
import unittest,json,os,requests
import getpathInfo
import paramunittest
from common import readExcel

result_path = os.path.join(getpathInfo.get_Path(),'result')
file_name = os.path.basename(__file__).split('.')[0]+'.xlsx'
excel  = readExcel.readExcel().get_xls(file_name, 'login')

@paramunittest.parametrized(*excel)     #参数化后可以单用例函数自行循环取用例进行执行
class Test_Login(unittest.TestCase):
    def setUp(self) -> None:
        self.url = 'http://apptest.qmxfs.com/v003'
        self.headers = {
        "Content-Type": "application/json",
        "clientName": "app_mall",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36"
    }
    def setParameters(self, case_name, path, payload,check):
        self.case_name = str(case_name)
        self.path = str(path)
        self.payload = str(payload)
        self.check = int(check)
    def test_01(self):
        all_path = self.url+self.path
        result = requests.post(url=all_path,data=self.payload,headers=self.headers).json()
        #res = json.loads(result)
        #print(result)
        self.assertEqual(result['state'],self.check,self.case_name+'————执行失败:'+str(result))

if __name__ == '__main__':
    unittest.main()