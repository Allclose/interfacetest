#dep登录测试用例脚本
import unittest,json,os
import requests
import getpathInfo
import paramunittest

#http://127.0.0.1:8888/login?name=xiaoming&pwd=111
from common import readExcel

url = 'http://127.0.0.1:8888/login/?'
header = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Accept-Encoding": "gzip, deflate",
    "Cookie": "schedulerToken=feb1a4e73416e05948bde646e9c01273; UUMS=f42ac75e-d07a-42f3-b80c-28108f4f21ca"
    }
body = {'name': 'xiaoming', 'pwd': '111'}
result_path = os.path.join(getpathInfo.get_Path(),'result')
excel  = readExcel.readExcel().get_xls('userCase.xlsx', 'login')



@paramunittest.parametrized(*excel)     #参数化后可以单用例函数自行循环取用例进行执行
class test_login(unittest.TestCase):
    def setParameters(self, case_name, path, query, method,check):
        self.case_name = str(case_name)
        self.path = str(path)
        self.query = str(query)
        self.method = str(method)
        self.check = int(check)
    def test_01(self):
        self.checkResult()
    '''def test_02(self):
        self.checkResult(url='http://127.0.0.1:8888/login?name=xiaoming&pwd=113',check=-1)
    def test_03(self):
        self.checkResult(url='http://127.0.0.1:8888/login?name=xiaoming&pwd=114',check=10001)'''
    def checkResult(self):

        url = 'http://127.0.0.1:8888'+self.path+'?'+self.query
        result = requests.get(url=url).text
        res = json.loads(result)
        #print(res['code'])
        self.assertEqual(res['code'],self.check,'失败')

if __name__ == '__main__':
    unittest.main()
    '''suite = unittest.TestSuite()
    #suite.addTest(test_login('test_01'))
    suite.addTests(map(test_login,['test_01']))
    #运行和报告
    time = time.strftime('%Y_%m_%d_%H_%M_%S')
    st = r'../result/sport'+str(time)+'.html'
    fp =  open(st,'wb')
    runner = HTMLTestRunner.HTMLTestRunner(stream=fp,title='测试报告',description='情况',verbosity=2)
    runner.run(suite)
    fp.close()'''