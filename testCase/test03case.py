#多接口测试用例
import unittest,json,requests

class TestCamp(unittest.TestCase):
    campId = None
    headers = {
        "Content-Type": "application/json",
        "clientName": "app_mall",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36"
    }
    def setUp(self) -> None:
        print("test03case测试开始...")
    #    self.campId = None
    def tearDown(self) -> None:
        print("test03case测试结束...")

    #获取训练营列表并返回最热训练营ID
    def test_0_trainCampList(self):
        path = "http://apptest.qmxfs.com/v003/trainingCamp/trainCampList"
        payload = {
                  "size": 10,
                  "page": 1,
                  "listType": 0,
                  "position": 1
                }
        result = requests.post(url=path,data=json.dumps(payload),headers=headers).json()
        #print(result)
        TestCamp.campId = result["data"]["list"][0]['campId']
        self.assertEqual(result['state'],1,'获取训练营列表失败')

    #根据训练营ID获取其课程目录
    def test_1_trainingCampCatalog(self):
        path = "http://apptest.qmxfs.com/v003/trainingCamp/trainingCampInfo"
        payload = {
                  "campId": TestCamp.campId,
                  "size": 20,
                  "page": 1,
                  "listType": 1,
                  "leavingMsg": ""
                }
        result = requests.post(url=path, data=json.dumps(payload), headers=headers).json()
        #print(TestCamp.campId)
        #print(result['state'])
        self.assertEqual(result['state'], 1, '获取训练营列表失败')
    def test_2_trainingCampCatalog(self):
        path = "http://apptest.qmxfs.com/v003/trainingCamp/trainingCampInfo"
        payload = {
                  "campId": TestCamp.campId,
                  "size": 20,
                  "page": 1,
                  "listType": 1,
                  "leavingMsg": ""
                }
        result = requests.post(url=path, data=json.dumps(payload), headers=headers).json()
        #print(TestCamp.campId)
        #print(result['state'])
        self.assertEqual(result['state'], 1, '获取训练营列表失败')
if __name__ == '__main__':
    unittest.main(verbosity=2)