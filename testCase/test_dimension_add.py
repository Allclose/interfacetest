import json,paramunittest,unittest,requests
from common.readYaml import ReadYaml
from common.configHttp import RunMain
case_list = ReadYaml.readyaml('dimension_add.yaml')

url = 'http://172.20.81.26:8001'
path = '/api/dep-dmm/dimension'
Cookie = "UUMS=c55ad1da-e50b-4414-be5e-3ebf9cff5225; tenantId=800; orgCode=803000000"
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36",
    "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
    "Content-Type": "application/json;charset=UTF-8",
    "Cookie": Cookie
}

@paramunittest.parametrized(*case_list)
class test_Dimension_add(unittest.TestCase):
    def setParameters(self,case_name,data,method,check):
        self.case_name = str(case_name)
        self.data = data        #raw类型含中文，直接使用json传入
        self.method = str(method)
        self.check = int(check)
    def description(self):
        self.case_name
    def setUp(self) -> None:
        print(self.case_name+"测试开始前准备")
    def tearDown(self) -> None:
        print("测试结束")
    def test_01_add(self):
        add_url = url+path

        result =requests.post(url=add_url,headers=headers,json=self.data).json()
        #res = RunMain().run_main(self.method, add_url, self.data)# 根据Excel中的method调用run_main来进行requests请求，并拿到响应
        status = result['status']# 将响应转换为字典格式
        print(result)
        self.assertEqual(status,self.check)

if __name__ == '__main__':
    unittest.main(verbosity=2)