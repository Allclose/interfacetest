import json,unittest,paramunittest,urllib.parse
from common.configHttp import RunMain
import geturlParams
from common.readYaml import ReadYaml
url = geturlParams.geturlParams().get_Url()# 调用我们的geturlParams获取我们拼接的URL    http://127.0.0.1:8888/login?

#通过EXcel记录测试用例
#import readExcel
#login_xls = readExcel.readExcel().get_xls('userCase.xlsx', 'login')

#通过Yaml记录测试用例
list = ReadYaml.readyaml('test01case.yaml')
@paramunittest.parametrized(*list) #参数化获取用例列表
class testUserLogin(unittest.TestCase):
    def setParameters(self, case_name, data,  method,check):
        self.case_name = str(case_name)
        self.data = str(data)
        self.method = str(method)
        self.check = int(check)
    def description(self):
        self.case_name
    def setUp(self):
        print(self.case_name+"测试开始前准备")
    def test01case(self):
        self.checkResult()
    def tearDown(self):
        print("测试结束，输出log完结\n\n")
    def checkResult(self):# 断言
        new_url = url + self.data
        data1 = dict(urllib.parse.parse_qsl(urllib.parse.urlsplit(new_url).query))# 将一个完整的URL中的name=&pwd=转换为{'name':'xxx','pwd':'bbb'}
        info = RunMain().run_main(self.method, url, data1)# 根据Excel中的method调用run_main来进行requests请求，并拿到响应
        ss = json.loads(info)# 将响应转换为字典格式
        self.assertEqual(ss['code'],self.check)
if __name__ == '__main__':
    unittest.main(verbosity=2)