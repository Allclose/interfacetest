import HTMLTestRunner
import unittest


class runAll:
    #读取文件，获取要执行的用例名称
    def set_case(self):
        fp = open('../caselist.txt')
        self.case_list = []
        for i in fp.readlines():
            if i.startswith('#') or i == '':
                continue
            else:
                case = i.split('/')[-1].replace('\n','')
                self.case_list.append(case)
        return self.case_list
        fp.close()
        #print(fp)
        #print(self.case_list)
    def set_suite(self):
        self.case_list = self.set_case()
        print(self.case_list)
        self.suite_mode = []
        for i in self.case_list:
            discover = unittest.defaultTestLoader.discover(start_dir=r'E:\Test\interfaceTest\testCase',
                                                           pattern=i + '.py', top_level_dir=None)
            self.suite_mode.append(discover)
        # print(self.suite_mode)
        self.suite = unittest.TestSuite()
        if len(self.suite_mode) >0:
            for suite in self.suite_mode:
                for test_name in suite:
                    self.suite.addTest(test_name)
                    #print(test_name)
                    #print('1')
            return self.suite
        else:
            return None

    def run(self):
        suite = self.set_suite()
        fp = open('../result/report.html', 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp,title='Test report')
        runner.run(suite)
if __name__ == '__main__':
    runAll().run()
