import os,time,common.Log,unittest,getpathInfo
from XTestRunner import HTMLTestRunner,SMTP
from common import readConfig
#调度
#import pythoncom
from apscheduler.schedulers.blocking import BlockingScheduler

path = getpathInfo.get_Path()
report_path = os.path.join(path, 'result')
on_off = readConfig.ReadConfig().get_email('on_off')
log = common.Log.logger
#以一个test01case脚本为一个用例
class AllTest:  # 定义一个类AllTest
    def __init__(self):  # 初始化一些参数和数据
        global resultPath
        resultPath = os.path.join(report_path, "report_"+str(time.strftime('%Y_%m_%d_%H_%M_%S'))+".html")  # result/report.html
        self.caseListFile = os.path.join(path, "caselist.txt")  # 配置执行哪些测试文件的配置文件路径
        self.caseFile = os.path.join(path, "testCase")  # 真正的测试脚本文件路径
        self.caseList = []

    def set_case_list(self):
        #读取caselist.txt文件中的用例名称，并添加到caselist元素组
        with open(self.caseListFile, 'r') as fp:
            for value in fp.readlines():
                data = str(value)
                if data != '' and not data.startswith("#"):  # 如果data非空且不以#开头
                    self.caseList.append(data.replace("\n", ""))  # 读取每行数据会将换行转换为\n，去掉每行数据中的\n

    def set_case_suite(self):
        self.set_case_list()  # 通过set_case_list()拿到caselist元素组
        test_suite = unittest.TestSuite()
        for case in self.caseList:  # 从caselist元素组中循环取出本次运行的脚本文件名
            case_name = case.split("/")[-1]  # 通过split函数来将aaa/bbb分割字符串，-1取后面，0取前面
            # 批量加载用例，第一个参数为用例存放路径，第一个参数为路径文件名
            discover = unittest.defaultTestLoader.discover(self.caseFile, pattern=case_name+'.py', top_level_dir=None)
            test_suite.addTests(discover)
        if test_suite:
            return test_suite
        else:
            return None
    def run(self):
        try:
            suit = self.set_case_suite()  # 调用set_case_suite获取test_suite
            if suit is not None:  # 判断test_suite是否为空
                with (open(resultPath,'wb')) as fp:  # 打开result/20181108/report.html测试报告文件，如果不存在就创建
                    # 调用XTestRunner的HTMLTestRunner
                    runner = HTMLTestRunner(
                        stream=fp,
                        title='接口测试报告',
                        description='用例执行情况',
                        language='en')
                    runner.run(
                        testlist=suit
                    )
            else:
                print("Have no case to test.")
        except Exception as ex:
            print(str(ex))
            log.info(str(ex))
        finally:
            print("*********TEST END*********")
            log.info("*********TEST END*********")
        # 判断邮件发送的开关
        if on_off == 'on':
            # 发邮件功能
            # 使用126邮箱发送时password应为授权码而非用户密码，须在邮箱客户端设置开启授权码
            # 使用gmail邮箱发送时password为用户密码，须在gmail客户端开启安全性较低的应用的访问权限
            smtp = SMTP(user="sender@qq.com", password="xxx", host="smtp.qq.com")
            smtp.sender(to="fnngj@126.com", subject="XTestRunner测试邮件", attachments=report_path)
        else:
            print("邮件发送开关配置关闭，请打开开关后可正常自动发送测试报告")

#调度运行
'''
pythoncom.CoInitialize()
scheduler = BlockingScheduler()
scheduler.add_job(AllTest().run, 'cron', day_of_week='1-5', hour=14, minute=50)
scheduler.start()'''

if __name__ == '__main__':
    AllTest().run()

