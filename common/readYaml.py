import yaml,os

import getpathInfo  # 自己定义的内部类，该类返回项目的绝对路径


class ReadYaml:
    def readyaml(case_file_name):
        path = os.path.join(getpathInfo.get_Path(), "testFile", case_file_name)
        with open(path, 'r',encoding='utf-8') as fp:
            data = yaml.safe_load(fp)
        return data['cases']

if __name__ == '__main__':  # 我们执行该文件测试一下是否可以正确获取Excel中的值
    print(ReadYaml.readyaml('dimension_add.yaml'))